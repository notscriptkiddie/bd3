import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'home',
	templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit{
	public title:string;
	public subtitle:string;

	constructor(){
		this.title = 'Facultad de Ingeniería de Sistemas e Informática'
		this.subtitle = 'Universidad Nacional Mayor de San Marcos 2019';
	}

	ngOnInit(){
	}
}