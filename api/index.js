'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3800;

// Conexión Database
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/red_social', { useMongoClient: true})
		.then(() => {
			console.log("La conexión a la base de datos se ha realizado correctamente.");
		
			app.listen(port, () => {
				console.log("Servidor corriendo en http://localhost:3800");
			});
		})
		.catch(err => console.log(err));